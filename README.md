# stocat - stochastic cat, selecting lines with uniform probability

## SYNOPSIS

- **stocat** \[**-p**|**--probability** PROBABILITY\] \[**-s**|**--seed** SEED\] \[_FILE_|**-**\]...

## DESCRIPTION

Concatenate FILE(s) to standard output, but printing each input line to output
only with a given probability, defaulting to 0.1 (i.e., 10%).

With no FILE or when FILE is **-**, read standard input.

## OPTIONS

- -p, --probability

    Output lines with the given probability, specified as a number between 0 (0%
    probability) and 1 (100% probability). Default: 0.1 (i.e., 10% probability).

- -s, --seed

    Seed the random number generator with a given integer (that will be passed
    to `srand()`), making sampling repeatable. Default: no seeding, using
    Perl's default seed initialization, which is generally time-dependent.

## SEE ALSO

[cat(1)](http://man.he.net/man1/cat)

## AUTHOR

Copyright 2020-2022 by Stefano Zacchiroli <zack@upsilon.cc>

Licensed under the GNU GPL, version 2 or above.
