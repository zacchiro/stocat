# Depends: perl

all: stocat.1
stocat.1: stocat
	pod2man $< > $@
clean:
	rm stocat.1

.PHONY: all clean
